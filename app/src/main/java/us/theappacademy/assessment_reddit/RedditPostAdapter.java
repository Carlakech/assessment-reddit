package us.theappacademy.assessment_reddit;


import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class RedditPostAdapter extends RecyclerView.Adapter<RedditPostHolder> {

    private ArrayList<ToDoItem> ToDoItem;
    private ActivityCallBack activityCallBack;

    public RedditPostAdapter(ActivityCallBack activityCallBack) {
        this.activityCallBack = activityCallBack;
    }

    @Override
    public RedditPostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new RedditPostHolder(view);
    }

    @Override
    public void onBindViewHolder(RedditPostHolder holder, final int position) {
        holder.titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //activityCallback.onPostSelected(redditUri);
            }
        });
        holder.titleText.setText(.get(position).title);
    }

    @Override
    public int getItemCount() {
        return redditPosts.size();
    }
}
