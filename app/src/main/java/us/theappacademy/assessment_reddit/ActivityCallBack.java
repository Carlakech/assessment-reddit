package us.theappacademy.assessment_reddit;


import android.net.Uri;

public interface ActivityCallBack {
    void onPostSelected(Uri redditPostUri);
}
