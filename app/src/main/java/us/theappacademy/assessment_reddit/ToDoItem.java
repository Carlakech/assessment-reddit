package us.theappacademy.assessment_reddit;

        public class ToDoItem {
            public String title;
            public String added;
            public String due;
            public String work;

            public ToDoItem(String title, String added, String due, String work) {
                this.title = title;
                this.added = added;
                this.due = due;
                this.work = work;
             }
        }